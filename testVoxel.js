/**
 * Created by jonasvhm on 5/16/17.
 */
class Transform{
    constructor(){
        this.position = vec3(0,0,0);
        this.rotation = vec3(0,0,0);
        this.scale = vec3(1,1,1);
    }

    translate(pos){
        this.position[0] += pos[0];
        this.position[1] += pos[1];
        this.position[2] += pos[2];
    }
    setPosition(pos){
        this.position[0] = pos[0];
        this.position[1] = pos[1];
        this.position[2] = pos[2];
    }
    rotate(rot){
        this.rotation[0] += rot[0];
        this.rotation[1] += rot[1];
        this.rotation[2] += rot[2];
    }
    setRotation(rot){
        this.rotation[0] = rot[0];
        this.rotation[1] = rot[1];
        this.rotation[2] = rot[2];
    }

    setScale(scal){
        this.scale[0] = scal[0];
        this.scale[1] = scal[1];
        this.scale[2] = scal[2];
    }

    setUniformScale(scal){
        this.scale[0] = scal;
        this.scale[1] = scal;
        this.scale[2] = scal;
    }
    getTransform(){
        let matPosition = translate(this.position[0],this.position[1],this.position[2]);
        let matRotation = mult(rotateZ(this.rotation[2]),mult(rotateY(this.rotation[1]),rotateX(this.rotation[0])));
        let matScale = scalem(this.scale[0],this.scale[1],this.scale[2]);
        let trans = mult(mat4(),mult(matPosition,mult(matRotation,matScale)));
        return trans;
    }
}

class testVoxel extends Transform {
    constructor(vertices,normals,texCoords,indices) {
        super();
        this.vertices = vertices;
        this.normals = normals;
        this.texCoords = texCoords;
        this.indices = indices;

        this.numVertices = 24;
        this.numIndices = 36;

        let cubeVerts =
            [
                -0.5, -0.5, -0.5,
                -0.5, -0.5,  0.5,
                0.5, -0.5,  0.5,
                0.5, -0.5, -0.5,
                -0.5,  0.5, -0.5,
                -0.5,  0.5,  0.5,
                0.5,  0.5,  0.5,
                0.5,  0.5, -0.5,
                -0.5, -0.5, -0.5,
                -0.5,  0.5, -0.5,
                0.5,  0.5, -0.5,
                0.5, -0.5, -0.5,
                -0.5, -0.5, 0.5,
                -0.5,  0.5, 0.5,
                0.5,  0.5, 0.5,
                0.5, -0.5, 0.5,
                -0.5, -0.5, -0.5,
                -0.5, -0.5,  0.5,
                -0.5,  0.5,  0.5,
                -0.5,  0.5, -0.5,
                0.5, -0.5, -0.5,
                0.5, -0.5,  0.5,
                0.5,  0.5,  0.5,
                0.5,  0.5, -0.5,
            ];
        let cubeNormals =
            [
                0.0, -1.0, 0.0,
                0.0, -1.0, 0.0,
                0.0, -1.0, 0.0,
                0.0, -1.0, 0.0,
                0.0, 1.0, 0.0,
                0.0, 1.0, 0.0,
                0.0, 1.0, 0.0,
                0.0, 1.0, 0.0,
                0.0, 0.0, -1.0,
                0.0, 0.0, -1.0,
                0.0, 0.0, -1.0,
                0.0, 0.0, -1.0,
                0.0, 0.0, 1.0,
                0.0, 0.0, 1.0,
                0.0, 0.0, 1.0,
                0.0, 0.0, 1.0,
                -1.0, 0.0, 0.0,
                -1.0, 0.0, 0.0,
                -1.0, 0.0, 0.0,
                -1.0, 0.0, 0.0,
                1.0, 0.0, 0.0,
                1.0, 0.0, 0.0,
                1.0, 0.0, 0.0,
                1.0, 0.0, 0.0
            ];
        let cubeTex =
            [
                0.0, 0.0,
                0.0, 1.0,
                1.0, 1.0,
                1.0, 0.0,
                1.0, 0.0,
                1.0, 1.0,
                0.0, 1.0,
                0.0, 0.0,
                0.0, 0.0,
                0.0, 1.0,
                1.0, 1.0,
                1.0, 0.0,
                0.0, 0.0,
                0.0, 1.0,
                1.0, 1.0,
                1.0, 0.0,
                0.0, 0.0,
                0.0, 1.0,
                1.0, 1.0,
                1.0, 0.0,
                0.0, 0.0,
                0.0, 1.0,
                1.0, 1.0,
                1.0, 0.0
            ];

        if ( this.vertices != null )
        {
            this.vertices.length = 3 * this.numVertices;
            for (let i = 0; i < this.numVertices * 3; i++ )
            {
                this.vertices[i] = cubeVerts[i] * this.scale[0];
            }
        }

        if ( this.normals != null )
        {
            this.normals.length = 3 * this.numVertices;
            for (let i = 0; i < this.numVertices * 3; i++ )
            {
                this.normals[i] = cubeNormals[i];
            }
        }

        if ( this.texCoords != null )
        {
            this.texCoords.length = 2 * this.numVertices;
            for (let i = 0; i < this.numVertices * 2; i++ )
            {
                this.texCoords[i] = cubeTex[i];
            }
        }

        if ( this.indices != null )
        {
            let cubeIndices =
                [
                    0, 2, 1,
                    0, 3, 2,
                    4, 5, 6,
                    4, 6, 7,
                    8, 9, 10,
                    8, 10, 11,
                    12, 15, 14,
                    12, 14, 13,
                    16, 17, 18,
                    16, 18, 19,
                    20, 23, 22,
                    20, 22, 21
                ];
            this.indices.length = this.numIndices;
            for (let i=0;i<this.numIndices;i++){
                this.indices[i] = cubeIndices[i];
            }
        }

    }
}