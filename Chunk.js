/**
 * Created by jonasvhm on 5/15/17.
 */
class Chunk{
    constructor(pos,amount){
        this.amount = amount;
        var positions = [];
        var indices = [];
        this.userData = gl.userData;
        var voxel = new testVoxel(positions,null,null,indices);
        this.userData.numIndices = voxel.numIndices;

        this.userData.indicesIBO = gl.createBuffer();
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER,this.userData.indicesIBO);
        gl.bufferData(gl.ELEMENT_ARRAY_BUFFER,new Uint16Array(voxel.indices),gl.STATIC_DRAW);
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER,null);

        this.userData.positionVBO = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER,this.userData.positionVBO);
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(voxel.vertices),gl.STATIC_DRAW);


        let colors = new Float32Array(this.amount * 4);
        for (let instance = 0; instance < this.amount; instance++ )
        {
            let icolors = colors.subarray(instance*4);
            icolors[0] = Math.random();
            icolors[1] = Math.random();
            icolors[2] = Math.random();
            icolors[3] = 1;
        }
        this.userData.colorVBO = gl.createBuffer ();
        gl.bindBuffer ( gl.ARRAY_BUFFER, this.userData.colorVBO );
        gl.bufferData ( gl.ARRAY_BUFFER, colors, gl.STATIC_DRAW );


        this.userData.mvpVBO = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER,this.userData.mvpVBO);
        this.userData.matrixBuf = new Float32Array(16*this.amount);
        gl.bufferData(gl.ARRAY_BUFFER,this.userData.matrixBuf,gl.DYNAMIC_DRAW);

        gl.bindBuffer(gl.ARRAY_BUFFER,null);
    }
    draw(){
        for(let i = 0;i<this.amount;i++){
            let voxel = new testVoxel(null,null,null,null);
            voxel.translate(new vec3(i,i,i));
            this.userData.matrixBuf = mult(this.userData.matrixBuf.subarray(16*this.amount),camera.perspective,voxel.getTransform());
            gl.bufferData(gl.ARRAY_BUFFER,this.userData.matrixBuf,gl.DYNAMIC_DRAW);
        }
    }
}