/**
 * Created by jonasvhm on 5/9/17.
 */
class Transform{
    constructor(){
        this.position = vec3(0,0,0);
        this.rotation = vec3(0,0,0);
        this.scale = vec3(0,0,0);
    }

    translate(pos){
        this.position[0] += pos[0];
        this.position[1] += pos[1];
        this.position[2] += pos[2];
    }
    setPosition(pos){
        this.position[0] = pos[0];
        this.position[1] = pos[1];
        this.position[2] = pos[2];
    }
    rotate(rot){
        this.rotation[0] += rot[0];
        this.rotation[1] += rot[1];
        this.rotation[2] += rot[2];
    }
    setRotation(rot){
        this.rotation[0] = rot[0];
        this.rotation[1] = rot[1];
        this.rotation[2] = rot[2];
    }

    scalem(scal){
        this.scale[0] += scal[0];
        this.scale[1] += scal[1];
        this.scale[2] += scal[2];
    }

    setScale(scal){
        this.scale[0] = scal[0];
        this.scale[1] = scal[1];
        this.scale[2] = scal[2];
    }
    getTransform(){
        let trans = mult(mat4(),mult(this.position,mult(this.rotation,this.scale)));
        return trans;
    }
}