/**
 * Created by jonasvhm on 5/8/17.
 */
class Camera{

    constructor(FOV,aspect,near,far){
        this.rot = new vec3(0,0,0);
        this.pos = new vec3(0,1.8,0);
        this.projection = perspective(FOV,aspect,near,far);
        this._update()
    }
    translate(pos){
        this.pos[0] += pos[0];
        this.pos[1] += pos[1];
        this.pos[2] += pos[2];
        this._update();
    }
    position(pos){
        this.pos[0] = pos[0];
        this.pos[1] = pos[1];
        this.pos[2] = pos[2];
        this._update();
    }
    rotate(rot){
        if(rot[1]>89.0){
            rot[1] = 89;
        }
        if(rot[1]<-89.0){
            rot[1]=-89.0;
        }
        this.rot[0] += rot[0];
        this.rot[1] += rot[1];
        this.rot[2] += rot[2];
        this._update();
    }
    rotation(rot){
        if(rot[1]>89.0){
            rot[1] = 98;
        }
        if(rot[1]<-89.0){
            rot[1]=-89.0;
        }
        this.rot[0] = rot[0];
        this.rot[1] = rot[1];
        this.rot[2] = rot[2];
        this._update();
    }
    _update(){
        let f = mult(rotateY(this.rot[1]),[0,0,-1,0]);
        let r = cross(f, vec3(0,1,0));
        this.front = normalize(f);
        this.back = this.invert(this.front);
        this.right = normalize(r);
        this.left = this.invert(this.right);
        this.view = mult(mult(rotateX(-this.rot[0]), rotateY(-this.rot[1])), translate(-this.pos[0],-this.pos[1],-this.pos[2]));
    }

    invert(temp){
    let inverted = new vec4();
        for(var i = 0; i<temp.length;i++){
            inverted[i] = -temp[i];
        }
    return inverted;
    }
}