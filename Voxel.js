/**
 * Created by jonasvhm on 5/9/17.
 */
class Transform{
    constructor(){
        this.position = vec3(0,0,0);
        this.rotation = vec3(0,0,0);
        this.scale = vec3(1,1,1);
    }

    translate(pos){
        this.position[0] += pos[0];
        this.position[1] += pos[1];
        this.position[2] += pos[2];
    }
    setPosition(pos){
        this.position[0] = pos[0];
        this.position[1] = pos[1];
        this.position[2] = pos[2];
    }
    rotate(rot){
        this.rotation[0] += rot[0];
        this.rotation[1] += rot[1];
        this.rotation[2] += rot[2];
    }
    setRotation(rot){
        this.rotation[0] = rot[0];
        this.rotation[1] = rot[1];
        this.rotation[2] = rot[2];
    }

    setScale(scal){
        this.scale[0] = scal[0];
        this.scale[1] = scal[1];
        this.scale[2] = scal[2];
    }

    setUniformScale(scal){
        this.scale[0] = scal;
        this.scale[1] = scal;
        this.scale[2] = scal;
    }
    getTransform(){
        let matPosition = translate(this.position[0],this.position[1],this.position[2]);
        let matRotation = mult(rotateZ(this.rotation[2]),mult(rotateY(this.rotation[1]),rotateX(this.rotation[0])));
        let matScale = scalem(this.scale[0],this.scale[1],this.scale[2]);
        let trans = mult(mat4(),mult(matPosition,mult(matRotation,matScale)));
        return trans;
    }
}

class Voxel extends Transform{
    constructor(){
        super();
        this.vertices = [
            // Front face
            -1.0, -1.0,  1.0,
            1.0, -1.0,  1.0,
            1.0,  1.0,  1.0,
            -1.0,  1.0,  1.0,

            // Back face
            -1.0, -1.0, -1.0,
            -1.0,  1.0, -1.0,
            1.0,  1.0, -1.0,
            1.0, -1.0, -1.0,

            // Top face
            -1.0,  1.0, -1.0,
            -1.0,  1.0,  1.0,
            1.0,  1.0,  1.0,
            1.0,  1.0, -1.0,

            // Bottom face
            -1.0, -1.0, -1.0,
            1.0, -1.0, -1.0,
            1.0, -1.0,  1.0,
            -1.0, -1.0,  1.0,

            // Right face
            1.0, -1.0, -1.0,
            1.0,  1.0, -1.0,
            1.0,  1.0,  1.0,
            1.0, -1.0,  1.0,

            // Left face
            -1.0, -1.0, -1.0,
            -1.0, -1.0,  1.0,
            -1.0,  1.0,  1.0,
            -1.0,  1.0, -1.0
        ];

        this.colors = [];

        for (var j = 0; j < this.vertices.length; j++) {
            this.colors = this.colors.concat(new vec4(1,1,1,1));
        }

        this.indices = [
            0,  1,  2,      0,  2,  3,    // front
            4,  5,  6,      4,  6,  7,    // back
            8,  9,  10,     8,  10, 11,   // top
            12, 13, 14,     12, 14, 15,   // bottom
            16, 17, 18,     16, 18, 19,   // right
            20, 21, 22,     20, 22, 23    // left
        ];

        this.vertexBufferId = this._createArrayBuffer(this.vertices);
        this.colorBufferId = this._createArrayBuffer(this.colors);
        this.elementArrayId = this._createElementArrayBuffer(this.indices);
        this.shaderId = initShaders(gl, 'Shaders/vertexShader.glsl', 'Shaders/fragmentShader.glsl'); //initialize the vertex and fragment shader
        gl.useProgram(this.shaderId); //tell webgl to use the specific shader
        var projectionAttributeLocation = gl.getUniformLocation(this.shaderId, "projection"); //find the projection matrix in the shader
        gl.uniformMatrix4fv(projectionAttributeLocation,false,flatten(camera.projection)); //set the projection matrix of the shader to that of the camera
    }

    draw(){
        gl.useProgram(this.shaderId); //tell webgl to use the specific shader
        var viewAttributeLocation = gl.getUniformLocation(this.shaderId, "view");
        gl.uniformMatrix4fv(viewAttributeLocation, false, flatten(camera.view));
        // hook up vertex buffer to shader
        gl.bindBuffer(gl.ARRAY_BUFFER, this.vertexBufferId);
        var vertexSize = 3;
        var vertexAttributeLocation = gl.getAttribLocation(this.shaderId, "vertex");
        gl.enableVertexAttribArray(vertexAttributeLocation);
        gl.vertexAttribPointer(vertexAttributeLocation, vertexSize, gl.FLOAT, false, 0, 0);

        // hook up vertex buffer to shader
        gl.bindBuffer(gl.ARRAY_BUFFER, this.colorBufferId);
        var colorSize = 4;
        var colorAttributeLocation = gl.getAttribLocation(this.shaderId, "color");
        gl.enableVertexAttribArray(colorAttributeLocation);
        gl.vertexAttribPointer(colorAttributeLocation, colorSize, gl.FLOAT, false, 0, 0);

        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.elementArrayId);

        var modelAttributeLocation = gl.getUniformLocation(this.shaderId, "model");
        gl.uniformMatrix4fv(modelAttributeLocation,false,flatten(this.getTransform()));
        gl.drawElements(gl.TRIANGLES, this.indices.length, gl.UNSIGNED_SHORT, 0);
    }

    _createArrayBuffer(array){
        // convert to typed array
        console.log(array);
        var floatArray = new Float32Array(array);
        console.log(floatArray);
        // create buffer id
        var id = gl.createBuffer();
        // set id to the current active array buffer (only one can be active)
        gl.bindBuffer(gl.ARRAY_BUFFER, id);
        // upload buffer data
        var hints = gl.STATIC_DRAW;
        gl.bufferData(gl.ARRAY_BUFFER, floatArray, hints);
        return id;
    }

    _createElementArrayBuffer(array){
        // convert to typed array
        var intArray = new Uint16Array(array);
        // create buffer id
        var id = gl.createBuffer();
        // set id to the current active array buffer (only one can be active)
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, id);
        // upload buffer data
        var hints = gl.STATIC_DRAW;
        gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, intArray, hints);
        return id;
    }
}