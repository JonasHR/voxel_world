/**
 * Created by jonasvhm on 5/15/17.
 */
var KeyCode = {
    W: 87,
    S: 83,
    A: 65,
    D: 68,
    SPACE: 32,
    SHIFT: 16,
};

class Input{

    constructor(sensitivity = 0.05){
        this.keys = {};
        this.firstMouse = true;
        this.sensitivity = sensitivity;
        this.lastX = 0;
        this.lastY = 0;
        this.offset = new vec2(0,0);
    }

    keyDown(event){
        for(var key in KeyCode){
            if(event.keyCode === KeyCode[key]){
                this.keys[KeyCode[key]] = true;
            }
        }
    }
    keyUp(event){
        for(var key in KeyCode){
            if(event.keyCode === KeyCode[key]){
                this.keys[KeyCode[key]] = false;
            }
        }
    }

    mouseMove(event){
        let havePointerLock = 'pointerLockElement' in document || 'mozPointerLockElement' in document || 'webkitPointerLockElement' in document;
        if(this.firstMouse){
            this.lastX = event.clientX;
            this.lastY = event.clientY;
            this.firstMouse = false;
        }
        let xOffset = event.clientX - this.lastX;
        let yOffset = this.lastY - event.clientY;

        this.lastX = event.clientX;
        this.lastY = event.clientY;

        if(havePointerLock){

            xOffset = event.movementX;
            yOffset = event.movementY;
        }

        xOffset*=this.sensitivity;
        yOffset*=this.sensitivity;
        this.offset[0] = yOffset;
        this.offset[1] = xOffset;
    }
}